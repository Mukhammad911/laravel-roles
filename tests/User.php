<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dushanbesoft\Roles\Traits\HasRoleAndPermission;

class User extends Model
{
    use HasRoleAndPermission;
}
